use std::fmt::{self, Display};
use std::str::FromStr;

use super::ParseError;

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct Prefix {
    pub origin: String,
    pub username: Option<String>,
    pub host: Option<String>,
}

impl Prefix {
    pub fn new(origin: impl Into<String>) -> Self {
        Self {
            origin: origin.into(),
            username: None,
            host: None,
        }
    }

    pub fn with_username(mut self, username: impl Into<String>) -> Self {
        self.username = Some(username.into());
        self
    }

    pub fn with_host(mut self, host: impl Into<String>) -> Self {
        self.host = Some(host.into());
        self
    }
}

impl Display for Prefix {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.origin)?;

        if let Some(username) = &self.username {
            write!(f, "!{}", username)?;
        }

        if let Some(host) = &self.host {
            write!(f, "@{}", host)?;
        }

        Ok(())
    }
}

impl FromStr for Prefix {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Prefix, ParseError> {
        let mut parts = s.splitn(2, '!');
        let origin = parts.next().ok_or(ParseError)?;
        let mut prefix = Prefix::new(origin);

        if let Some(rest) = parts.next() {
            let mut parts = rest.splitn(2, '@');
            prefix.username = parts.next().map(String::from);
            prefix.host = parts.next().map(String::from);
        }

        Ok(prefix)
    }
}
