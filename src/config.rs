use std::time::Duration;

#[derive(Clone, serde::Deserialize)]
pub struct Config {
    pub db: DbConfig,
    pub irc: IrcConfig,
    pub bot: BotConfig,
}

#[derive(Clone, serde::Deserialize)]
pub struct IrcConfig {
    pub server: String,
    pub port: u16,
    pub tls: bool,
    pub channels: Vec<String>,
    pub nickname: String,
    pub username: String,
    pub realname: String,
    #[serde(default)]
    pub nick_password: Option<String>,
}

#[derive(Clone, serde::Deserialize)]
pub struct DbConfig {
    pub url: String,
    pub max_pool_size: u32,
    pub min_pool_size: u32,
    #[serde(with = "humantime_serde")]
    pub connect_timeout: Duration,
    #[serde(default, with = "humantime_serde")]
    pub max_lifetime: Option<Duration>,
    #[serde(default, with = "humantime_serde")]
    pub idle_timeout: Option<Duration>,
    #[serde(default)]
    pub test_on_acquire: bool,
}

#[derive(Clone, serde::Deserialize)]
pub struct BotConfig {
    pub prefix: String,
    pub bots: Vec<String>,
}
