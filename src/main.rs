pub mod bot;
pub mod command;
pub mod config;
pub mod handlers;
pub mod irc;
pub mod query;
pub mod util;

use self::bot::Bot;
use self::config::Config;
use self::irc::Client;

use anyhow::{Context, Result};
use sqlx::Pool;

fn load_config() -> Result<Config> {
    let contents = std::fs::read_to_string("hubblest.toml")?;
    let config = toml::from_str(&contents)?;
    Ok(config)
}

#[tokio::main(basic_scheduler)]
async fn main() -> Result<()> {
    env_logger::init();

    let config = load_config()?;

    let mut irc = Client::connect(&config.irc.server, config.irc.port, config.irc.tls)
        .await
        .context("Connection to IRC server failed")?;

    let db = Pool::builder()
        .max_size(config.db.max_pool_size)
        .min_size(config.db.min_pool_size)
        .connect_timeout(config.db.connect_timeout)
        .idle_timeout(config.db.idle_timeout)
        .max_lifetime(config.db.max_lifetime)
        .test_on_acquire(config.db.test_on_acquire)
        .build(&config.db.url)
        .await
        .context("Connection to database failed")?;

    irc.nick(&config.irc.nickname).await?;
    irc.user(&config.irc.username, &config.irc.realname).await?;

    let mut trial = 0i32;

    loop {
        let msg = irc.recv().await?;
        match msg.command.as_str() {
            "001" => break,
            "433" => {
                trial += 1;
                irc.nick(format!("{}{}", &config.irc.nickname, trial))
                    .await?;
            }
            _ => (),
        }
    }

    if let Some(ref pass) = config.irc.nick_password {
        if trial > 0 {
            irc.privmsg(
                "NICKSERV",
                format!("GHOST {} {}", &config.irc.nickname, pass),
            )
            .await?;
            irc.nick(&config.irc.nickname).await?;
        }

        irc.privmsg("NICKSERV", format!("IDENTIFY {}", pass))
            .await?;
    }

    irc.join(config.irc.channels.join(",")).await?;

    let mut bot = Bot::new(config, irc, db);
    bot.harvest_handlers();
    bot.run().await
}
