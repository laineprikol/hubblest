use anyhow::Result;
use reqwest::Client;

const URL: &str = "https://trashbin.bpm140.ru/";

pub async fn paste(data: String) -> Result<String> {
    let client = Client::new();
    let res = client
        .post(format!("{}addplain", URL).as_str())
        .body(data)
        .send()
        .await?;
    let text = res.text().await?;
    let pasteid = text.lines().next().unwrap_or("").to_owned();
    Ok(format!("{}{}", URL, pasteid))
}
