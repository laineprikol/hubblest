use anyhow::Result;

use crate::irc::Client;
use crate::query::{CountResult, Message, TopResult};
use crate::util::paste;

fn format_result_short(urls: &[String], messages: &[Message]) -> Vec<String> {
    let url_len = urls.iter().map(|v| v.len()).max().unwrap_or(0);
    let author_len = messages.iter().map(|v| v.author.len()).max().unwrap_or(0);
    let indent = format!("\x0303{} │\x0F ", " ".repeat(url_len + author_len + 1));

    let mut lines = Vec::with_capacity(messages.len());

    for (i, msg) in messages.iter().enumerate() {
        let mut wrap = textwrap::wrap_iter(&msg.body, 80);
        lines.push(format!(
            "\x0310{:url_len$}\x0303 {:>author_len$} │\x0F {}",
            urls[i],
            msg.author,
            wrap.next().unwrap(),
            url_len = url_len,
            author_len = author_len,
        ));

        for part in wrap {
            lines.push(format!("{}{}", indent, part));
        }
    }

    lines
}

fn format_result_long(messages: &[Message]) -> Vec<String> {
    let author_len = messages.iter().map(|v| v.author.len()).max().unwrap_or(0);

    messages
        .iter()
        .map(|msg| {
            format!(
                "{:45} {:>width$} | {}",
                msg_url(msg),
                msg.author,
                msg.body,
                width = author_len,
            )
        })
        .collect()
}

pub async fn send_search_result(
    client: &mut Client,
    target: &str,
    messages: Vec<Message>,
) -> Result<()> {
    if messages.is_empty() {
        return client.privmsg(target, "\x0304no results").await;
    }

    let short = messages[..3.min(messages.len())].to_owned();
    let urls = short.clone().into_iter().take(3).map(|m| msg_url(&m)).collect::<Vec<String>>();

    for line in format_result_short(&urls, &short) {
        client.privmsg(target, line).await?;
    }

    if messages.len() > 3 {
        let long_url = paste(format_result_long(&messages).join("\n")).await?;
        client
            .privmsg(target, format!(" ... \x0310{}", long_url))
            .await?;
    }

    Ok(())
}

fn msg_url(msg: &Message) -> String {
    format!(
        "https://logs.fomalhaut.me/{}.log#{}",
        msg.timestamp.format("%Y-%m-%d"),
        msg.offset
    )
}

fn format_top(result: TopResult) -> Vec<String> {
    let count_len = result
        .top
        .iter()
        .map(|v| v.1.to_string().len())
        .max()
        .unwrap_or(0);
    let nickname_len = result.top.iter().map(|v| v.0.len()).max().unwrap_or(0);
    let top = result.top.iter().enumerate();

    if result.top.len() < 6 {
        return top
            .map(|(i, (author, count))| {
                format!(
                    "{}. \x02{:>count_len$}\x0F {}",
                    i + 1,
                    count,
                    author,
                    count_len = count_len,
                )
            })
            .collect();
    }

    top.map(|(i, (author, count))| {
        let line = format!(
            "{}. \x02{:>count_len$}\x0F {:<nickname_len$}  │ ",
            i + 1,
            count,
            author,
            count_len = count_len,
            nickname_len = nickname_len
        );

        match i {
            0 => format!("{}\x02Total users:", line),
            1 => format!("{}  {}", line, result.total_users),
            2 => format!("{}\x02Total users (w/o aliases):", line),
            3 => format!("{}  {}", line, result.total_users_raw),
            4 => format!("{}\x02Total messages:", line),
            5 => format!("{}  {}", line, result.total_messages),
            _ => line,
        }
    })
    .collect()
}

pub async fn send_top_result(client: &mut Client, target: &str, result: TopResult) -> Result<()> {
    if result.top.is_empty() {
        return client.privmsg(target, "\x0304no results").await;
    }

    for line in format_top(result) {
        client.privmsg(target, line).await?;
    }

    Ok(())
}

pub async fn send_count_result(
    client: &mut Client,
    target: &str,
    result: CountResult,
) -> Result<()> {
    client
        .privmsg(
            target,
            format!(
                "\x02Messages:\x0F {:8}, \x02Users:\x0F {:4}, \x02Users (w/o aliases):\x0F {:4}",
                result.total_messages, result.total_users, result.total_users_raw
            ),
        )
        .await
}
