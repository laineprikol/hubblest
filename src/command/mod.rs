use anyhow::{anyhow, Result};
use async_trait::async_trait;

use crate::bot::{BotContext, Handler};
use crate::irc::Message;

#[async_trait]
pub trait CommandHandler {
    fn name(&self) -> &str;

    async fn handle(&mut self, bot: &mut BotContext, command: String, target: String)
        -> Result<()>;
}

#[async_trait]
impl<T: CommandHandler + Clone + Send + 'static> Handler for T {
    fn clone(&self) -> Box<dyn Handler> {
        Box::new(self.clone())
    }

    fn can_handle(&self, bot: &mut BotContext, msg: &Message) -> bool {
        if msg.command != "PRIVMSG" || msg.params.len() < 2 {
            return false;
        }

        let msg = &msg.params[1];
        let prefix = &bot.config.bot.prefix;
        if !msg.starts_with(prefix) {
            return false;
        }

        let msg = &msg[prefix.len()..];
        if !msg.starts_with(self.name()) {
            return false;
        }

        let msg = &msg[self.name().len()..];
        msg.is_empty() || msg.starts_with(' ')
    }

    async fn handle(&mut self, bot: &mut BotContext, msg: Message) -> Result<()> {
        let prefix = &bot.config.bot.prefix;
        let target = msg.response_target().ok_or(anyhow!("bad privmsg"))?;
        let command = msg.params[1][prefix.len() + self.name().len()..]
            .trim_start()
            .to_owned();

        let result = CommandHandler::handle(self, bot, command, target.to_owned()).await;

        if let Err(e) = result {
            log::error!("{:?}", e);
            bot.irc
                .privmsg(target, format!("\x0304error:\x0F {}", e))
                .await?;
        }

        Ok(())
    }
}

macro_rules! command {
    ($name:literal, $func:ident) => {
        mod $func {
            use super::*;

            #[allow(non_camel_case_types)]
            #[derive(Clone)]
            pub struct $func;

            impl crate::command::CommandHandler for $func {
                fn name(&self) -> &str {
                    $name
                }

                fn handle<'life0, 'life1, 'async_trait>(
                    &'life0 mut self,
                    bot: &'life1 mut BotContext,
                    command: String,
                    target: String,
                ) -> ::core::pin::Pin<
                    Box<
                        dyn ::core::future::Future<Output = Result<()>>
                            + ::core::marker::Send
                            + 'async_trait,
                    >,
                >
                where
                    'life0: 'async_trait,
                    'life1: 'async_trait,
                    Self: 'async_trait,
                {
                    async fn run(
                        _self: &$func,
                        bot: &mut BotContext,
                        command: String,
                        target: String,
                    ) -> Result<()> {
                        super::$func(bot, command, target).await
                    }
                    Box::pin(run(self, bot, command, target))
                }
            }

            inventory::submit!({
                let handler: Box<dyn crate::bot::Handler> = Box::new($func);
                handler
            });
        }
    };
}

mod alias;
mod help;
mod import;
mod search;
