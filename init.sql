CREATE EXTENSION pg_trgm;

CREATE TABLE messages (
    msg_id          SERIAL PRIMARY KEY,
    msg_timestamp   TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    msg_offset      INTEGER NOT NULL,
    msg_channel     TEXT NOT NULL,
    msg_author      TEXT NOT NULL,
    msg_body        TEXT NOT NULL
);

CREATE TABLE aliases (
    alias_primary   TEXT NOT NULL,
    alias_secondary TEXT NOT NULL,
    PRIMARY KEY     (alias_primary, alias_secondary)
);

CREATE INDEX messages_fts  ON messages USING gin(to_tsvector('russian', msg_body));
CREATE INDEX messages_trgm ON messages USING gin(msg_body gin_trgm_ops);